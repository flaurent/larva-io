Larva IO & QC
=============

**larva-io** features basic *load* and *save* routines for data files derived from larva experiment.

This includes *.spine* and *.outline* ascii files, *trx.mat* mat files, and others to come.

Installation
------------

Python >= 3.6 is required.

The **larva-io** package is not available on PyPI yet. You can get a copy of it and install it locally::

        git clone git@gitlab.pasteur.fr:flaurent/larva-io.git
        cd larva-io
	pip install .

*pip install* will install some Python dependencies if missing, but you may still need to install the `HDF5 reference library <https://support.hdfgroup.org/downloads/index.html>`_.

Basic usage
-----------

For each supported format in the `larva.io` package, three functions are provided:

* `load` that loads the entire data from a file into a format that depends on the backend for this file format,
* `save` that exports data to a file,
* `check` that runs a series of quality control checks on the data.

For example, the `larva.io.chore` subpackage exports `load_spine`, `load_outline`, `save_spine`, etc.

All such functions exhibit the same base interface:

.. code::python

        data = load_spine(input_filepath)

        save_spine(output_filepath, data)

        check_spine(filepath)
        # or:
        check_spine(data)
        # or else:
        check_spine(data, datasource=filepath)

These functionalities are also available as type classes. For example, `larva.io.chore` exports the `Spine`  and `Outline` classes:

.. code::python

        spine = Spine(input_filepath)
        data = spine.load()

        spine = Spine(output_filepath)
        spine.save(data)

        spine = Spine(filepath)
        spine.check()
        spine.check(data)

        spine = Spine()
        spine.check(data)

Data quality control
--------------------

The `check` function or method also admits keyword argument `policy`.
Per default, `check` raises a `QCFailure` error on the first failure to comply with a quality control check (`policy='fail'`).

Any value other than `'fail'` (or default `None`) lets all the checks run, and `check` returns comprehensive diagnosis information.

The `policy` argument also admits value `'fix'` which makes each check try to fix the data, possibly removing the data elements that do not comply.

Not all the checks can fix the data. In this case, a check only adds diagnosis information and passes on the data to the next check.

Not all the file formats or backends include quality control.

Lazily loading data
-------------------

In addition to the simple `load` and `save` functions and methods, a backend may feature extra functionalities, especially to lazily process data.

For example, the `larva.io.trx` package provides the `Trx` class with a `list_varnames` method, and equivalently a `list_trx_varnames` function.
