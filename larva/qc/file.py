from pathlib import Path
from .check import QCChecks

class QCFileBackend:

    __slots__ = '_load', '_save', '_default_qc_checks'

    def __init__(self, load, save, default_qc_checks):
        if not callable(load):
            raise TypeError('load is not callable')
        if not (callable(save) or save is NotImplemented):
            raise TypeError('save is not callable')
        self._load = load
        self._save = save
        self.default_qc_checks = default_qc_checks

    @property
    def load(self):
        return self._load

    @property
    def save(self):
        return self._save

    @property
    def default_qc_checks(self):
        return self._default_qc_checks

    @default_qc_checks.setter
    def default_qc_checks(self, qc_checks):
        if qc_checks is None:
            qc_checks = QCChecks()
        elif isinstance(qc_checks, (tuple, list)):
            qc_checks = QCChecks(qc_checks)
        self._default_qc_checks = qc_checks

class QCFile:
    """
    Datafile with attached quality control.
    """

    __slots__ = 'filepath', '_qc', '_backend'

    def __init__(self, filepath=None):
        self.filepath = filepath
        self._backend = None
        self._qc = None

    @property
    def backend(self):
        if self._backend is None:
            cls = type(self)
            raise NotImplementedError(f'{cls} does not select a default backend')
        return self._backend

    @backend.setter
    def backend(self, backend):
        self._qc = None
        self._backend = backend

    @property
    def qc(self):
        if self._qc is None:
            cls = type(self)
            # make a copy
            self._qc = QCChecks(self.backend.default_qc_checks)
        return self._qc

    @qc.setter
    def qc(self, val):
        if isinstance(val, (tuple, list, QCChecks)):
            self._qc = QCChecks(val)
        elif val is None:
            self._qc = QCChecks()
        else:
            raise AttributeError('not a list')

    def load(self, *args, **kwargs):
        return self.backend.load(self.filepath, *args, **kwargs)

    def check(self, data=None, policy=None, **kwargs):
        # make data lazily available
        if data is None:
            data = self.load(**kwargs)
        # run the checks, fix the data and collect the diagnoses
        diagnoses = dict()
        for check in self.qc:
            data, diagnosis = check(data,
                    datasource=self.filepath, policy=policy)
            if diagnosis:
                diagnoses[check.get_name()] = diagnosis
        #
        return data, diagnoses

    def save(self, data, *args, **kwargs):
        self.backend.save(self.filepath, data, *args, **kwargs)

    def __getattr__(self, attr):
        return self.qc.__getattr__(attr)

def asfun(cls, met):
    def _fun(filepath, *args, **kwargs):
        self = cls(filepath)
        return met(self, *args, **kwargs)
    _fun.__doc__ = met.__doc__
    return _fun

def load(cls):
    return asfun(cls, cls.load)

def save(cls):
    return asfun(cls, cls.save)

def check(cls):
    def _check(filepath_or_data, policy=None, **kwargs):
        if isinstance(filepath_or_data, (str, Path)):
            filepath = filepath_or_data
            self = cls(filepath)
            data = None
        else:
            self = cls()
            data = filepath_or_data
        return self.check(data, policy, **kwargs)
    _check.__doc__ = cls.check.__doc__
    return _check

__all__ = [
        'QCFileBackend',
        'QCFile',
        'load',
        'check',
        'save',
        ]
