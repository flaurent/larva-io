Quality control for larva-related data
======================================

A quality control function tests for a single desired or undesired
feature of the data, and may report any lack of compliance.

A full-featured qc function will be structured as follows:

.. code::python

    @qc_check
    def my_check(controller, data, my_check_extra_argument0=None, ...):

        # crawl the data
        for item in data:

            # check for compliance
            ...

            # if `item` does not comply
            controller.report(my_diagnostic_key0=my_diagnostic_value0, ...)

            if controller.tryfix():
                # modify `data`
                ...

        return modified_data

Such a function takes a *controller* object as input argument, and can query
this object for directions to follow, especially in the case the data can be
fixed, whether the function should fix the data or not.

