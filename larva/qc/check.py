import logging
from .exceptions import QCFailure

class QCController:
    """
    Controller for quality control checks.

    An instance is passed as first argument to each quality control
    function.

    The main features a quality control function can access are:

    * logging, with methods `debug`, `info`, `warn` and `error`,
    * reporting of data item that do not comply (`report`),
    * asking whether to fix the items that do not comply (`tryfix`).

    Calling `report` may raise an error or return depending on the
    selected policy.
    """

    def __init__(self, qccheck=None, datasource=None):
        self._logger = None
        self.datasource = datasource
        self.qccheck = qccheck
        self.records = []

    @property
    def qccheck(self):
        raise AttributeError('write-only property')

    @qccheck.setter
    def qccheck(self, check):
        if check is None:
            self.qcname = None
            self.policy = None
            self.args = {}
        else:
            self.qcname = check.get_name()
            self.policy = check.policy
            #self.args = dict(check.args)
            self.args = {}

    @property
    def logger(self):
        """
        logging.Logger:
            Logging utility, whose main methods are also exposed by the
            `QCController` object
        """
        if self._logger is None:
            self._logger = logging.getLogger(__name__)
            if not self._logger.hasHandlers():
                self._logger.setLevel(logging.INFO)
                self._logger.addHandler(logging.StreamHandler())
        return self._logger

    @logger.setter
    def logger(self, logger):
        self._logger = logger

    def format(self, kwargs):
        src = self.datasource
        name = self.qcname
        return f"in file '{src}', check '{name}' failed with: {kwargs}"

    def report(self, **kwargs):
        """
        Register the input key-value pairs for diagnostic and either
        raise a `QCFailure` error to interrupt the quality control, or
        silently return to allow for qc continuation.

        Every quality control function is free to define its own
        diagnosis keys.
        """
        self.records.append(kwargs)
        if self.policy is None or self.policy == 'fail':
            raise QCFailure(self.format(kwargs))
        elif 'notify' in self.policy:
            logger = self.logger.warn(self.format(kwargs))

    def tryfix(self):
        """
        Boolean method that returns :const:`True` if fixing the data
        items that do not comply is desired, :const:`False` otherwise.

        A quality control function is not expected to call this method
        if no fixing strategies are implemented.
        """
        return 'fix' in self.policy

    def diagnose(self):
        """
        Return a compilation of all reported key-value pairs.
        """
        return list(self.records)

    @property
    def debug(self):
        """
        method: Shortcut to the `logger.debug` method
        """
        return self.logger.debug

    @property
    def info(self):
        """
        method: Shortcut to the `logger.info` method
        """
        return self.logger.info

    @property
    def warn(self):
        """
        method: Shortcut to the `logger.warn` method
        """
        return self.logger.warn

    @property
    def error(self):
        """
        method: Shortcut to the `logger.error` method
        """
        return self.logger.error

class QCCheck:
    """
    Wrapper for control quality functions.

    A `QCCheck` object is callable, but the type signature differs from
    that of the wrapped function.

    Especially, it supports two different signatures:

    * if the data are passed as a positional argument (more generally
      if any positional argument is passed), the wrapped function is
      called with a `QCController` instance as first input argument and
      the data (or datasource) as second argument, plus additional
      keyword arguments. If no `QCFailure` error is raised, the
      modified data are returned as first output argument and a
      diagnosis `dict` as second output argument.

    * if only keyword arguments are passed, they are stored and passed
      to the wrapped function on each subsequent call to this function.

    """

    __slots__ = (
            'fun', 'args',
            'input_type',
            'default_policy', 'active_policy',
            '_ctrl_impl',
            )

    def __init__(self, fun, policy=None, input_type=None):
        self.fun = fun
        self.args = {}
        self.input_type = input_type
        self.default_policy = policy
        self.active_policy = None
        self._ctrl_impl = None

    def get_name(self):
        return self.fun.__name__

    @property
    def policy(self):
        if self.active_policy is None:
            return self.default_policy
        else:
            return self.active_policy

    @property
    def _controller(self):
        ctrl_cls = QCController if self._ctrl_impl is None else self._ctrl_impl
        return ctrl_cls()

    @_controller.setter
    def _controller(self, ctrl):
        if ctrl is None or isinstance(ctrl, type):
            impl = ctrl
        else:
            impl = type(ctrl)
        self._ctrl_impl = impl

    def get_controller(self, datasource=None):
        ctrl = self._controller
        ctrl.datasource = datasource
        ctrl.qccheck = self
        return ctrl

    def __enter__(self):
        if self.active_policy is not None:
            log = self._controller.logger.warn
            log('active_policy already set; will be overwritten')
        return self

    def __exit__(self, *args):
        self.active_policy = None

    def __call__(self, *args, datasource=None, **kwargs):
        if args:
            try:
                input_data, = args
            except ValueError:
                raise ValueError('too many positional arguments') from None
            if self.input_type == 'datasource':
                input_data = datasource
            elif callable(input_data):
                input_data = input_data()
            fun_kwargs = {
                    kw: arg \
                    for kw, arg in self.args.items() \
                    if arg is not None
                    }
            fun_kwargs.update(kwargs)
            self.active_policy = fun_kwargs.pop('policy', None)
            ctrl = self.get_controller(datasource)
            ctrl.args = fun_kwargs
            output_data = self.fun(ctrl, input_data, **fun_kwargs)
            if output_data is None:
                if self.input_type == 'datasource':
                    output_data = args[0]
                else:
                    output_data = input_data
            return output_data, ctrl.diagnose()
        else:
            self.args.update(kwargs)

    def copy(self):
        cls = type(self)
        fork = cls(self.fun)
        fork.args = dict(self.args)
        fork.input_type = self.input_type
        fork._ctrl_impl = self._ctrl_impl
        return fork

    def __getattr__(self, attr):
        return getattr(self.fun, attr)

class OrderedSet:
    """
    Sequence of unique elements (or sequence-like set) that preserves
    insertion order.

    An `OrderedSet` can also be viewed as an `OrderedDict` for element
    types that can contain the keys.

    An element is translated into a key with the `get_elem_id` function.
    Such a function should return an id value that is not an `int`, not
    to be confused with indices, and should also differ from the element
    type.

    Unlike `set` or other typical set types, the elements passed on
    initialization are assumed to be unique.

    Uniqueness is checked only on insertion methods such as `insert` or
    `add`.
    """

    __slots__ = '_elems', '_get_elem_id', '_id_checked'

    def __init__(self, elems=[], get_elem_id=None):
        if isinstance(elems, OrderedSet):
            if get_elem_id is None:
                self._get_elem_id = elems._get_elem_id
                self._id_checked = elems._id_checked
                self._elems = list(elems.elems)
                return
            else:
                elems = elems.elems
        self.get_elem_id = get_elem_id
        self.elems = list(elems)

    @property
    def get_elem_id(self):
        """
        callable: Function that takes an element and returns an id
        """
        return self._get_elem_id

    @get_elem_id.setter
    def get_elem_id(self, get_id):
        self._id_checked = False
        if get_id is None:
            get_id = id
        self._get_elem_id = get_id

    @property
    def elems(self):
        """
        list:
            Sequence of unique elements;
            setting `elems` does not check for duplicate values
        """
        return self._elems

    @elems.setter
    def elems(self, elems):
        self._elems = elems
        self._id_checked = False
        self._check_elem_and_id()

    def _check_elem_and_id(self):
        if not self._id_checked and self._elems:
            example_elem = self._elems[0]
            if isinstance(exemple_elem, int):
                raise TypeError('elem is int; cannot distinguish between indices and elems')
            example_id = self.get_elem_id(example_elem)
            if isinstance(example_id, int):
                raise TypeError('elem id is int; cannot distinguish between indices and ids')
            self._id_checked = True

    def __bool__(self):
        return bool(self.elems)

    def __len__(self):
        return len(self.elems)

    def __iter__(self):
        for elem in self.elems:
            yield elem

    def __contains__(self, elem):
        _id = self.get_elem_id(elem)
        for _elem in self.elems:
            if _id == self.get_elem_id(_elem):
                return True
        return False

    def __getitem__(self, index_or_id):
        if isinstance(index_or_id, int):
            index = index_or_id
            return self.elems[index]
        else:
            _id = index_or_id
            for elem in self.elems:
                if _id == self.get_elem_id(_elem):
                    return elem

    def index(self, elem):
        """
        Return the index of the first occurence of `elem`, or raise a
        `ValueError` exception if not found.
        """
        prefix, suffix = self.split(elem)
        if suffix:
            return len(prefix)
        else:
            _id = self.get_elem_id(elem)
            raise ValueError(f"cannot find elem '{_id}'")

    def split(self, elem):
        """
        Split the sequence of elements at the first occurence of `elem`.
        
        Two lists are returned so that the matching element is at the
        head of the second list.
        """
        _id = self.get_elem_id(elem)
        prefix, suffix = [], []
        for _elem in self.elems:
            if _id == self.get_elem_id(_elem):
                if suffix:
                    raise RuntimeError(f"elem '{_id}' appears multiple times")
                else:
                    suffix.append(_elem)
            else:
                if suffix:
                    suffix.append(_elem)
                else:
                    prefix.append(_elem)
        return prefix, suffix

    def reverse(self):
        """
        Reverse the sequence of elements. Works inplace.
        """
        self._elems = self.elems[::-1]

    def move(self, index, elem):
        """
        Move element `elem` to position `index` in the sequence.

        If `elem` is not found, a `ValueError` exception is raised.
        """
        if index < 0:
            self.reverse()
            try:
                self.move(1 - index, elem)
            finally:
                self.reverse()
        else:
            _id = self.get_elem_id(elem)
            found = False
            _elems, self._elems = self.elems, []
            try:
                for _index, _elem in enumerate(_elems):
                    if _id == self.get_elem_id(_elem):
                        if found:
                            raise RuntimeError(f"elem '{_id}' appears multiple times")
                        else:
                            found = True
                        if index == _index:
                            # already at the right location
                            self.elems.append(elem)
                    else:
                        if index == _index:
                            self.elems.append(elem)
                        self.elems.append(_elem)
                raise ValueError(f"cannot find elem '{_id}'")
            except:
                self._elems = _elems
                raise

    def insert(self, index, elem, fail_if_exists=False):
        """
        Insert element `elem` into the sequence at position `index`.

        Works inplace.
        """
        if elem in self:
            if fail_if_exists:
                _id = self.get_elem_id(elem)
                raise ValueError(f"elem '{_id}' already exists")
        else:
            self.elems.append(elem)
            self._check_elem_and_id()
            self.move(index, elem)

    def add(self, elem, fail_if_exists=False):
        """
        Add or append an element.

        If `elem` is found in the sequence, nothing happens. `elem` is
        appended to the right end of the sequence only if it is missing.

        Works inplace.
        """
        if elem in self:
            if fail_if_exists:
                _id = self.get_elem_id(elem)
                raise ValueError(f"elem '{_id}' already exists")
        else:
            self.elems.append(elem)
            self._check_elem_and_id()

    def __add__(self, set2):
        cls = type(self)
        set1 = self
        if not isinstance(set2, cls):
            cls = type(set2)
            if not isinstance(set1, cls):
                raise TypeError('not an OrderedSet')
        # check
        if set1.get_elem_id is not set2.get_elem_id:
            raise TypeError('ids do not compare')
        # copy
        set1 = cls(set1)
        # append
        for elem in set2:
            set1.add(elem)
        #
        return set1

class QCChecks(OrderedSet):
    """
    Series of quality control checks.

    Each checking function should be wrapped into a `QCCheck` instance.
    """

    __slots__ = ()

    def __init__(self, check_list, copy=True, check_id=QCCheck.get_name):
        if copy:
            check_list = [ check.copy() for check in check_list ]
        OrderedSet.__init__(self, check_list, check_id)

    def _check_elem_and_id(self):
        if not self._id_checked and self._elems:
            example_elem = self._elems[0]
            example_id = self.get_elem_id(example_elem)
            if not isinstance(example_id, str):
                raise TypeError('qc check name is not str')
            self._id_checked = True

    def __getattr__(self, check_name):
        try:
            return self[check_name]
        except KeyError as e:
            raise AttributeError(e.msg) from None

def qc_check(*args, default_policy=None, input_type=None):
    """
    Wrapping function intended to be used as a decorator:

    .. code::python

        @qc_check
        def my_data_check(controller, data):
            # check data
            ...

        @qc_check(input_type='datasource')
        def my_filepath_check(controller, datasource):
            # check datasource
            ...

        @qc_check(default_policy='fix')
        def my_data_fix(controller, data):
            # check data
            ...

            # on failure to comply
            controller.report(my_diagnosis_key=my_diagnosis_value, ...)

            if controller.tryfix():
                # fix data
                ...

            return data

    Per default, newly decorated quality control functions are NOT
    associated to any file type.
    Especially, they may be specific to a backend for the target file
    type.
    As a consequence, each backend defines its own quality control
    checks.
    """
    if args:
        fun = args[0]
        if args[1:] or not callable(fun):
            raise ValueError('@qc_check got positional arguments')
        return QCCheck(fun,
                policy=default_policy,
                input_type=input_type,
                )
    else:
        def _qc_check(fun):
            return qc_check(fun,
                    default_policy=default_policy,
                    input_type=input_type,
                    )
        return _qc_check

__all__ = [
        'OrderedSet',
        'QCController',
        'QCCheck',
        'QCChecks',
        'qc_check',
        ]
