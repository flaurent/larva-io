import pandas as pd

def load_spine(filepath):
    # load the file
    df = pd.read_csv(filepath, sep=' ', header=None)

    # give explicit names to key columns
    coord_cols = list(range(len(df.columns)-3))
    df.columns = ['date_time', 'larva_id', 'time'] + coord_cols

    return df

def save_spine(filepath, df, float_format='%.3f', ignore_nan=False):
    if not df.empty:
        if not ignore_nan and df.isnull().values.any():
            raise ValueError('NaN found; outline files not supported')
        if not float_format.startswith('%'):
            float_format = '%'+float_format
        df.to_csv(filepath,
                sep=' ', index=None, header=None, float_format=float_format)

from .check import \
        spine_outline_default_qc_checks,
        spine_specific_default_qc_checks,

from larva.qc.file import QCFileBackend

spine_backend = QCFileBackend(
        load_spine,
        save_spine,
        spine_outline_default_qc_checks + spine_specific_default_qc_checks,
        )

__all__ = ['load_spine', 'save_spine', 'spine_backend']
