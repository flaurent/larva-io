from .null_rows import null_rows
from .nan_columns import nan_columns
from .track_min_duration import track_min_duration
from .spine_outline_sync import spine_outline_sync

spine_outline_default_qc_checks = [
        nan_columns,
        null_rows,
        track_min_duration,
        ]

spine_specific_default_qc_checks = []

outline_specific_default_qc_checks = [
        spine_outline_sync,
        ]

__all__ = [
        'spine_outline_default_qc_checks',
        'spine_specific_default_qc_checks',
        'outline_specific_default_qc_checks',
        ]
