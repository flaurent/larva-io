from larva.qc import qc_check
import numpy as np

@qc_check
def track_min_duration(ctrl, df, min_duration=5):
    larva_ids = df['larva_id']
    ok = np.full(df.shape[0], True)
    for larva_id in np.unique(larva_ids):
        track = larva_ids==larva_id
        time = np.sort(df['time'][track])
        duration = time[-1] - time[0]
        if duration < min_duration:
            ctrl.report(larva_id=larva_id, duration=duration)
            if ctrl.tryfix():
                ok[track] = False
    return df[ok]
