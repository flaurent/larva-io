from larva.qc import qc_check
import numpy as np

@qc_check(default_policy='fix')
def nan_columns(ctrl, df):
    coord_cols = df.columns[3:]
    broken_col = np.all(np.isnan(df[coord_cols].values), axis=0)
    broken_cols = [ col for i, col in enumerate(coord_cols) if broken_col[i] ]
    if broken_cols:
        ctrl.report(cols=broken_cols)
        if ctrl.tryfix():
            df = df.drop(columns=broken_cols)
    return df
