from larva.qc import qc_check
import numpy as np

@qc_check
def null_rows(ctrl, df):
    coords = df[df.columns[3:]].values
    broken_row = np.all(coords == 0, axis=1)
    larva_ids = df['larva_id'].values
    broken_larvae = np.unique(larva_ids[broken_row])
    if 0 < broken_larvae.size:
        ctrl.report(larva_ids=broken_larvae)
        if ctrl.tryfix():
            ok = np.full(df.shape[0], True)
            for broken_larva in broken_larvae:
                discard_rows = larva_ids == broken_larva
                ok[discard_rows] = False
            df = df[ok]
    return df
