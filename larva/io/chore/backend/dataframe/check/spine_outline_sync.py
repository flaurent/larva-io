import numpy as np
from larva.qc import qc_check

@qc_check(input_type='datasource')
def spine_outline_sync(ctrl, datasource):
    from larva.io.chore.spine_outline import (
            find_associated_file, load_spine, load_outline,
            )
    file1 = datasource
    file2 = find_associated_file(file1)
    if file2 is None:
        ctrl.report(desc='no associated file found')
    else:
        # load_outline should work with spine files
        df1 = load_outline(file1)
        df2 = load_outline(file2)
        nrows1 = df1.shape[0]
        nrows2 = df2.shape[0]
        if nrows1 != nrows2:
            ctrl.report(nrows=(nrows1, nrows2))
