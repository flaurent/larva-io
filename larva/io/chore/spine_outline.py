import os
from larva.qc.file import QCFile, load, check, save

from .backend.dataframe import purepy as default

def find_associated_file(spine_or_outline_file):
    dirname, basename = os.path.split(spine_or_outline_file)
    if not dirname:
        dirname = '.'
    max_common_prefix_len = len(basename)
    longest_common_prefix_len = 0
    best_candidate = None
    for candidate in os.listdir(dirname):
        common_prefix_len = len(os.path.commonprefix((candidate, basename)))
        if longest_common_prefix_len < common_prefix_len and \
                common_prefix_len < max_common_prefix_len:
            longest_common_prefix_len = common_prefix_len
            best_candidate = candidate
    return best_candidate

class SpineOutline(QCFile):

    __slots__ = ()

    def find_associated_file(self):
        return find_associated_file(self.filepath)

class Spine(SpineOutline):

    __slots__ = ()

    def __init__(self, filepath=None):
        SpineOutline.__init__(self, filepath)
        self.backend = default.spine_backend

class Outline(SpineOutline):

    __slots__ = ()

    def __init__(self, filepath=None):
        SpineOutline.__init__(self, filepath)
        self.backend = default.outline_backend

load_spine = load(Spine)
load_outline = load(Outline)

check_spine = check(Spine)
check_outline = check(Outline)

save_spine = save(Spine)
save_outline = save(Outline)

__all__ = [
        'Spine',
        'Outline',
        'SpineOutline',
        'load_spine',
        'load_outline',
        'check_spine',
        'check_outline',
        'save_spine',
        'save_outline',
        ]
