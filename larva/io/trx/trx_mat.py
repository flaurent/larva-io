from larva.qc.file import QCFile, asfun, load, check, save
import h5py
from .backend import hdf5storage as default

class Trx(QCFile):

    __slots__ = ()

    def __init__(self, filepath=None):
        QCFile.__init__(self, filepath)
        self.backend = default.trx_backend

    def list_varnames(self):
        filepath = self.filepath
        if not filepath:
            raise ValueError('filepath is not defined')
        try:
            store = h5py.File(filepath, 'r')
        except OSError:
            raise
        else:
            varnames = list(store['trx'].keys())
            return varnames
        finally:
            store.close()

list_trx_varnames = asfun(Trx, Trx.list_varnames)
load_trx = load(Trx)
check_trx = check(Trx)
save_trx = save(Trx) # not implemented

__all__ = [
        'Trx',
        'list_trx_varnames',
        'load_trx',
        'check_trx',
        'save_trx',
        ]
