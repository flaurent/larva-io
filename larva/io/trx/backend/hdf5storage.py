import hdf5storage

def load_trx(filepath, varnames=[]):
    if varnames:
        if isinstance(varnames, str):
            varnames = varnames.split()
        trx = hdf5storage.loadmat(filepath,
                variable_names=[ 'trx/'+varname for varname in varnames ])
    else:
        trx = hdf5storage.loadmat(filepath)
    trx = { varname[4:]: trx[varname] for varname in trx }
    return trx

from larva.qc.file import QCFileBackend

trx_backend = QCFileBackend(
        load_trx,
        NotImplemented,
        [],
        )

__all__ = ['load_trx', 'trx_backend']
