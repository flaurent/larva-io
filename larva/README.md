Package structure
=================

*larva* contains two subpackages:

* [*io*](io):
  exposes subpackages for reading and writing files of different formats,

* [*qc*](qc):
  implements general quality control mechanics used by *io* subpackages.

